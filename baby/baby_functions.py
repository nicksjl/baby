# all the imports
import os
import io
import random
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import sqlite3
from baby.elo_calculation import elo_calculation
from datetime import datetime
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.dates import (YEARLY, DateFormatter,
                              rrulewrapper, RRuleLocator, drange)
                              


def score_verif(gamedate,player1,player2,player3,player4,score12,score34):
    
    commit = True
    flash = ""
    #Date
    try:
        np.datetime64(gamedate)
    except:
        commit=False
        flash='Wrong date format (YY-MM-DD)'
    #players
    for player in [player1,player2,player3,player4]:
        if player not in players_list():
            commit=False
            flash='player not in list'
    #score
    try :
        score12_int = int(score12)
    except:
         commit=False
         flash='score12 is not an integer'
    try :
        score34_int = int(score34)
    except:
        commit=False
        flash='score34 is not an integer'
    if score12 == score34: 
        commit=False
        flash = 'identical scores'
    return commit,flash

def elo_df(con):
    scores_df = pd.read_sql_query(
    'select gamedate, player1, player2, player3, player4, score12,'+\
    'score34 from entries order by gamedate desc',con)
    elos_df = elo_calculation(scores_df)



def vict_df(con):
    """
    Create a pd.DataFrame with scores from the database 
    """
    scores_df = pd.read_sql_query(
    'select gamedate, player1, player2, player3, player4, score12,'+\
    'score34 from entries order by gamedate desc',con) 
    elos_df = elo_calculation(scores_df)
    zeros = np.zeros((scores_df.shape[0],len(elos_df.columns)))
    vict_df = pd.DataFrame(zeros,
                            index = scores_df['gamedate'],
                            columns=elos_df.columns)
    print(elos_df.columns)
    for index, row in scores_df.iterrows():
        if row['score12']>row['score34']:
            vict_df.loc[row['gamedate']][row['player1']]+=1
            vict_df.loc[row['gamedate']][row['player2']]+=1
        else: 
            vict_df.loc[row['gamedate']][row['player3']]+=1
            vict_df.loc[row['gamedate']][row['player4']]+=1

    return vict_df
    
def create_elo_figure(elos_df):
    """
    @param elos_df *pd.DataFrame* : 
    @returns figure
    """
    fig = Figure()
    axis = fig.add_subplot(1, 1, 1)
    axis.set_xlabel('Date')
    axis.set_ylabel('ELO')
    axis.grid()

    elos_df=elos_df.sort_index()
    index_dateformat = [np.datetime64(date) for date in elos_df.index]
    
    iterator=0
    for column in elos_df.columns:
        iterator+=1
        if iterator<8:
            linestyle='solid'
        else:
            linestyle='dashed'
        axis.plot(index_dateformat, elos_df[column],label=column,linestyle=linestyle)

    plt.ylim(bottom=1500)
    #rule = rrulewrapper(YEARLY, byeaster=1, interval=5)
    #loc = RRuleLocator(rule)
    formatter = DateFormatter('%d/%m/%y')
    #axis.xaxis.set_major_locator(loc)
    axis.xaxis.set_major_formatter(formatter)
    axis.xaxis.set_tick_params(rotation=30, labelsize=10)

    box = axis.get_position()
    axis.set_position([box.x0, box.y0, box.width*0.75, box.height])

    axis.legend(bbox_to_anchor=(1.5, 1), loc='upper right', ncol=1)
    #plt.legend(bbox_to_anchor=(0, 1), loc='upper left', ncol=1)
    axis.set_ylim(bottom=1500)
    return fig


def players_list():
    return ['jacky','simon','florent','benoit',
            'jean-louis','guillaume','philippe',
            'louis','claire','jerome','nicolas',
            'luc','hadrien','julien','antoine',
            'marco','philippe']
