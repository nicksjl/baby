drop table if exists entries;
create table entries (
  id integer primary key autoincrement,
  gamedate date not null,
  player1 text not null,
  player2 text not null,
  player3 text not null,
  player4 text not null,
  score12 int not null,
  score34 int not null,
  comm text not null,
);
