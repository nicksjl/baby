# all the imports
import os
import sqlite3
import io
import random
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from baby.baby_functions import vict_df, score_verif, create_elo_figure
from baby.elo_calculation import elo_calculation
from datetime import datetime
from flask import Response
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash
from matplotlib.dates import (YEARLY, DateFormatter,
                              rrulewrapper, RRuleLocator, drange)
                              
app = Flask(__name__) # create the application instance :)
app.config.from_object(__name__) # load config from this file , flaskr.py

# Load default config and override config from an environment variable
app.config.update(dict(
    DATABASE=os.path.join(app.root_path, 'baby.db'),
    SECRET_KEY='development key',
    USERNAME='admin',
    PASSWORD='default'
))
app.config.from_envvar('BABY_SETTINGS', silent=True)

def connect_db():
    """Connects to the specific database."""
    rv = sqlite3.connect(app.config['DATABASE'])
    rv.row_factory = sqlite3.Row
    return rv
    
    
def get_db():
    """Opens a new database connection if there is none yet for the
    current application context.
    """
    if not hasattr(g, 'sqlite_db'):
        g.sqlite_db = connect_db()
    return g.sqlite_db

@app.teardown_appcontext
def close_db(error):
    """Closes the database again at the end of the request."""
    if hasattr(g, 'sqlite_db'):
        g.sqlite_db.close()

@app.cli.command('initdb')
def initdb_command():
    """Initializes the database."""
    db = get_db()
    with app.open_resource('schema.sql', mode='r') as f:
        db.cursor().executescript(f.read())
    db.commit()
    print('Initialized the database.')
 
@app.cli.command('initdbfromcsv')
def initdb_fromcsv_command():
    """Initializes the database."""
    csv_path = os.path.join(app.root_path, 'bf_scores_cplet_csv.csv')
    df = pd.read_csv(csv_path,header=0)
   
    df['id'] = df.index
    cols = df.columns.tolist()
    cols = cols[-1:] + cols[:-1]
    df = df[cols]
    df.columns = ['id','gamedate','player1','player2','player3','player4',
                    'score12','score34','comm']
    df['gamedate'] = df['gamedate'].str.replace('/','-')
    con = sqlite3.connect(app.config['DATABASE'])
    
    df.to_sql(name='entries',con = con ) 
#-----------------------------------------------------------------   
#LOGIN.HTML
#-----------------------------------------------------------------
@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username'
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password'
        else:
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(url_for('show_scores'))
    return render_template('login.html', error=error)
    
@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('show_scores'))

#-----------------------------------------------------------------   
#SHOW_SCORES.HTML
#-----------------------------------------------------------------
@app.route('/')
def show_scores():
    db = get_db()
    #con = sqlite3.connect(app.config['DATABASE'])
    scores_df = pd.read_sql_query(
    'select id, gamedate, player1, player2, player3, player4, score12,'+\
    'score34, comm from entries order by gamedate desc',db)
    scores_df=scores_df.set_index('id')
    scores_df = scores_df.sort_values(by = 'id', ascending=False)
    html = scores_df.to_html(classes='data', header=True)
    
    #cur = db.execute('select id, gamedate, player1, player2, '+\
    #                 'player3, player4, score12,'+\
    #                 'score34,comm from entries order by gamedate desc')
    #entries = cur.fetchall()
    #print(entries)
    #return render_template('show_scores.html', entries=entries)
    return render_template('show_scores.html', entries=[html])
#-----------------------------------------------------------------   
# ADD SCORE redirect show_scores.py
#-----------------------------------------------------------------
@app.route('/add', methods=['POST'])
def add_score_to_db():
    if not session.get('logged_in'):
        abort(401)
    gamedate = request.form['gamedate']
    if gamedate=="":
        gamedate = datetime.today().strftime('%Y-%m-%d')
    player1 = request.form['player1']
    player2 = request.form['player2']
    player3 = request.form['player3']
    player4 = request.form['player4']
    score12 = request.form['score12']
    score34 = request.form['score34']
    comment = request.form['comment']
    #------------------------------------------------
    # values verification
    #-------------------------------------------------
    commit,msg = score_verif(gamedate, player1, player2, 
                                player3, player4,score12,score34)
    if flash != "":
        flash(msg)
    #if all tests succeeded
    if commit:
        db = get_db()
        scores_df = pd.read_sql_query(
    'select id, gamedate, player1, player2, player3, player4, score12,'+\
    'score34, comm from entries order by id desc',db)
        #import pdb
        #pdb.set_trace()
        new_id = scores_df['id'][0]+1
        db.execute('insert into entries (id, gamedate, player1, player2, '+\
                   'player3, player4, '+\
                   'score12, score34, comm) values (?, ?, ?, ?, ?, ?, ?, ?, ?)',
                 [new_id,gamedate,player1,player2,player3,player4,score12,score34,comment])
        

        db.commit()
        flash('New entry was successfully posted')
    
    return redirect(url_for('show_scores'))

#-----------------------------------------------------------------   
# REMOVE SCORE redirect show_scores.py
#-----------------------------------------------------------------
@app.route('/remove', methods=['POST'])
def remove_score_from_db():
    id_to_remove = request.form['remove_id']
    con = sqlite3.connect(app.config['DATABASE'])
    scores_df = pd.read_sql_query(
    'select id, gamedate, player1, player2, player3, player4, score12,'+\
    'score34 from entries order by gamedate desc',con)
    if int(id_to_remove) not in scores_df.index:
        print(id_to_remove)
        print(scores_df.index)
        flash('id not in score list')
    else:
        db = get_db()
        try: 
            db.execute('DELETE FROM entries WHERE id={};'.format(id_to_remove))
        except sqlite3.Error as e :
            print ("Error {}:".format(e.args[0]))


        db.commit()
    con = sqlite3.connect(app.config['DATABASE'])
    scores_df2 = pd.read_sql_query(
    'select id, gamedate, player1, player2, player3, player4, score12,'+\
    'score34 from entries order by gamedate desc',con)
    #import pdb
    #pdb.set_trace()
    return redirect(url_for('show_scores'))


#-----------------------------------------------------------------   
#ELO.HTML
#-----------------------------------------------------------------
@app.route('/elos', methods=['POST','GET'])
def elos():
    con = sqlite3.connect(app.config['DATABASE'])
    elos_df = get_elo_df_from_db(con)
    df_last_day = elos_df.iloc[-1]
    df_last_day.transpose()
    df_last_day = df_last_day.sort_values(ascending=False)
    html = pd.DataFrame(df_last_day).to_html(classes='data', header=True)
    return render_template('elos.html', tables=[html])
 
#-----------------------------------------------------------------   
#PLOT ELO.PNG
#-----------------------------------------------------------------
@app.route('/plot_elo.png')
def plot_elo_png():
    con = sqlite3.connect(app.config['DATABASE'])
    elos_df = get_elo_df_from_db(con)
    fig = create_elo_figure(elos_df)
    output = io.BytesIO()
    FigureCanvas(fig).print_png(output)
    return Response(output.getvalue(), mimetype='image/png')
#-----------------------------------------------------------------   
#PLOT_VICT.PNG
#-----------------------------------------------------------------
@app.route('/plot_vict.png')
def plot_vic_png():
    con = sqlite3.connect(app.config['DATABASE'])
    output = io.BytesIO()
    FigureCanvas(fig).print_png(output)
    return Response(output.getvalue(), mimetype='image/png')
    
    
def get_elo_df_from_db(con):
    con = sqlite3.connect(app.config['DATABASE'])
    scores_df = pd.read_sql_query(
    'select gamedate, player1, player2, player3, player4, score12,'+\
    'score34 from entries order by gamedate desc',con)

    elos_df = elo_calculation(scores_df)
    
    return elos_df

