# all the imports
import os
import pandas as pd
import numpy as np
import math
import csv
import sys
import datetime
import pdb

def get_player_list(df_scores):
    p1_list = df_scores['player1'].drop_duplicates()
    p2_list = df_scores['player2'].drop_duplicates()
    p3_list = df_scores['player3'].drop_duplicates()
    p4_list = df_scores['player4'].drop_duplicates()
    players_list = pd.concat((p1_list,p2_list,p3_list,p4_list)).drop_duplicates()
    return players_list
 
def elo_calculation(df):
    get_player_list(df)
    
    #jours      =["Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi","Dimanche"]
    Date       =['2015-11-16']
    nb_date    =1
    elo_deb    =2000
    ELO        ={}
    ELO_equipes={}
    BN         ={}
    CSV        ={'match':[],'date':[],'e1j1':[],'e1j2':[],'e2j1':[],'e2j2':[],
             'score1':[],'score2':[],'comm':[]}
    rec_joueur =[elo_deb,elo_deb]
    rec_equipe =[elo_deb,elo_deb]
    nabsent    =50
    nJoursJoues=3*30
    nm=0
    df=df.sort_values(by='gamedate')
    for index, jeu in df.iterrows(): 
       
        date  =jeu["gamedate"]
        e1j1  =jeu["player1"]
        e1j2  =jeu["player2"]
        e2j1  =jeu["player3"]
        e2j2  =jeu["player4"]
        score1=int(jeu["score12"])
        score2=int(jeu["score34"])
        comm=""
        
        # On stocke tous les matchs lu
  
        nm=nm+1;
        CSV['match' ].append(nm)
        CSV['date'  ].append(date)
        CSV['e1j1'  ].append(e1j1)
        CSV['e1j2'  ].append(e1j2)
        CSV['e2j1'  ].append(e2j1)
        CSV['e2j2'  ].append(e2j2)
        CSV['score1'].append(score1)
        CSV['score2'].append(score2)
        CSV['comm'  ].append(comm)
        # On identifie la date
        d=-1
        # Nouvelle date ?
        if not date in Date :
            # On calcule les records du jour passe
            #F_records(rec_joueur,ELO,d)
            #F_records(rec_equipe,ELO_equipes,d)
            # On rajoute une colonne (en recopiant la derniere valeur)
            for nom in ELO.keys() :
                ELO[nom][-1].append(ELO[nom][-1][-1])
                ELO[nom][ 0].append(ELO[nom][ 0][-1])
                ELO[nom][ 1].append(ELO[nom][ 1][-1])
                ELO[nom][ 2].append(ELO[nom][ 2][-1])
            for nom in ELO_equipes.keys() :
                ELO_equipes[nom][-1].append(ELO_equipes[nom][-1][-1])
                ELO_equipes[nom][ 0].append(ELO_equipes[nom][ 0][-1])
                ELO_equipes[nom][ 1].append(ELO_equipes[nom][ 1][-1])
                ELO_equipes[nom][ 2].append(ELO_equipes[nom][ 2][-1])
            # On insere la date
            Date.append(date)
            # On incremente le compteur
            nb_date+=1
        # Nouveau nom ?
        F_ajoutJoueur(ELO,e1j1,elo_deb,nb_date)
        F_ajoutJoueur(ELO,e1j2,elo_deb,nb_date)
        F_ajoutJoueur(ELO,e2j1,elo_deb,nb_date)
        F_ajoutJoueur(ELO,e2j2,elo_deb,nb_date)
        if (not e1j1+" "+e1j2 in ELO_equipes.keys())&\
            (not e1j2+" "+e1j1 in ELO_equipes.keys()) :
            F_ajoutJoueur(ELO_equipes,e1j1+" "+e1j2,elo_deb,nb_date)
        if (not e2j1+" "+e2j2 in ELO_equipes.keys())&\
            (not e2j2+" "+e2j1 in ELO_equipes.keys()) :
            F_ajoutJoueur(ELO_equipes,e2j1+" "+e2j2,elo_deb,nb_date)
        # Determination du nouveau ELO
        
        elo_min=F_ELO(ELO,e1j1,e1j2,score1,e2j1,e2j2,score2,d,d)
        # On corrige le elo de debut pour les eventuels nouveaux joueurs
        # if nb_date > 5 :
        #   elo_deb=min(elo_deb,elo_min)
        # Pour les equipes, on cherche la bonne combinaison des noms
        if e1j1+" "+e1j2 in ELO_equipes :
            e1=e1j1+" "+e1j2
        else :
            e1=e1j2+" "+e1j1
        if e2j1+" "+e2j2 in ELO_equipes :
            e2=e2j1+" "+e2j2
        else :
            e2=e2j2+" "+e2j1
        # La prise en compte du score depend du nb de matchs de chaque
        # des joueurs et pas de celui de l'equipe
        ELO_equipes[e1][0][0]=min([ELO[e1j1][0][d-1],ELO[e1j2][0][d-1]])
        ELO_equipes[e2][0][0]=min([ELO[e2j1][0][d-1],ELO[e2j2][0][d-1]])
        F_ELO(ELO_equipes,e1,e1,score1,e2,e2,score2,d,0)
        # Determination des "betes noires" pour chaque joueur
        #F_BN(BN,e1j1,e1j2,score1,e2j1,e2j2,score2)
        # On stocke le numero du dernier match joue
        ELO[e1j1][4].append(nm)
        ELO[e2j1][4].append(nm)
        if e1j2!=e1j1:
            ELO[e1j2][4].append(nm)
        if e2j2!=e2j1:
            ELO[e2j2][4].append(nm)
        ELO_equipes[e1][4].append(nm)
        ELO_equipes[e2][4].append(nm)
    players = list(ELO.keys())
    elos0=[]
    elos1=[]
    elos2=[]
    for player,list_of_list in ELO.items():
        elos0.append(list_of_list[7])
        elos1.append(list_of_list[1])
        elos2.append(list_of_list[2])
    data = np.array(elos0)
    dataT= data.T
    df_elo = pd.DataFrame(index=Date,data=dataT,columns=players) 

    return df_elo
   
# DEBUT des fonctions =========================================================

# ---------------
# Fonction printf
# ---------------

def printf(format, *args):
  sys.stdout.write(format % args)

# ----------------
# Fonction sprintf
# ----------------

def sprintf(format, *args):
  return format % args

# Fonction K (facteur dependant du nb de partie jouee et du classement)
# K=40 pour les 30 premiers matchs
# K=20 pour des elo inferieurs a 2400
# K=10 pour les autres

def F_K(nb,elo) :
  if nb < 30 :
    K=40
  elif elo < 2400 :
    K=20
  else :
    K=10
  return K

# -----------------------------------------------
# Fonction pour ajouter un joueur (ou une equipe)
# -----------------------------------------------
# ELO     : dictionnaire contenant tous les joueurs
# nom     : joueur a ajouter
# elo_deb : ELO de depart
# nb      : nombre d'element

def F_ajoutJoueur(ELO,nom,elo_deb,nb) :
  # Besoin de l'ajouter ?
  if not nom in ELO.keys() :
    # Definition des noms des joueurs, du nombre de matchs joue (0), du
    # nb de victoires (1), du nb de buts marques lors des defaites (2), des
    # records (3 - status, perso min, perso max), du numero des matchs joues
    # (4), victoire ou defaite (5), l'evolution du ELO (6) et du elo (dernier
    # tuple obligatoirement)
    ELO[nom]=([0],[0],[0],[0,elo_deb,elo_deb],[0],[str()],[0],[0])
    # Pour les premiers match, je mets "elo_deb", sinon 0 (pour
    # l'affichage, pour ne pas avoir plusieurs lignes qui coupent
    # tout le graphe)
    if nb<10 : ELO[nom][-1][-1]=elo_deb
    for d in range(nb-1) :
      ELO[nom][-1].append(ELO[nom][-1][-1])
      ELO[nom][ 0].append(ELO[nom][ 0][-1])
      ELO[nom][ 1].append(ELO[nom][ 1][-1])
      ELO[nom][ 2].append(ELO[nom][ 2][-1])
    # Initialisation des deux derniers elo a "elo_deb"
    ELO[nom][-1][-1]=elo_deb
    ELO[nom][-1][-2]=elo_deb



# -------------------------
# Fonction de calcul du ELO
# -------------------------
# ELO    : dictionnaire contenant tous les joueurs
# e1j1   : equipe 1 - joueur 1
# e1j2   : equipe 1 - joueur 2
# score1 : score de l'equipe 1
# e2j1   : equipe 2 - joueur 1
# e2j2   : equipe 2 - joueur 2
# score2 : score de l'equipe 2
# d      : date a calculer
# dnb    : date indiquant le nombre de matchs pour definir l'affectation ou
#          non du score.
# Cette fonction retourne le plus petit des ELO calcule

def F_ELO(ELO,e1j1,e1j2,score1,e2j1,e2j2,score2,d,dnb) :
  # facteur lie a la difference de score
  # fac=abs(score1-score2)/5
  # Correction si les deux joueurs d'une meme equipe sont identiques
  if e1j1 == e1j2 :
    pond1=0.5
  else :
    pond1=1.0
  if e2j1 == e2j2 :
    pond2=0.5
  else :
    pond2=1.0
  # Calcul du elo moyen
  elo1=0.5*(ELO[e1j1][-1][d-1]+ELO[e1j2][-1][d-1])
  elo2=0.5*(ELO[e2j1][-1][d-1]+ELO[e2j2][-1][d-1])
  # Nouveau elo
  D12 =elo1-elo2
  D21 =elo2-elo1
  p12 =1/(1+math.pow(10,-D12/400))
  p21 =1/(1+math.pow(10,-D21/400))
  # K ?
  K11=F_K(ELO[e1j1][0][dnb],ELO[e1j1][-1][d-1])
  K12=F_K(ELO[e1j2][0][dnb],ELO[e1j2][-1][d-1])
  K21=F_K(ELO[e2j1][0][dnb],ELO[e2j1][-1][d-1])
  K22=F_K(ELO[e2j2][0][dnb],ELO[e2j2][-1][d-1])
  # On ne comptabilise les scores que si tous les joueurs ont un coefficient
  # K inferieur a 40, sinon, on ne comptabilise que pour les joueurs ayant
  # un coeff de 40
  if (K11==40)|(K12==40)|(K21==40)|(K22==40) :
    if K11!=40 : K11=0
    if K12!=40 : K12=0
    if K21!=40 : K21=0
    if K22!=40 : K22=0
  else :
    K11=K12=(K11+K12)/2
    K21=K22=(K21+K22)/2
  # Victoire ou defaite : correction des scores
  if score1 > score2 :
    delo_e1j1=round(K11*(1-p12))
    delo_e1j2=round(K12*(1-p12))
    delo_e2j1=round(K21*(0-p21))
    delo_e2j2=round(K22*(0-p21))
    # Incrementation des victoires
    ELO[e1j1][1][d]+=pond1
    ELO[e1j2][1][d]+=pond1
    # Incrementation du nb de but quand on perd
    ELO[e2j1][2][d]+=score2*pond2
    ELO[e2j2][2][d]+=score2*pond2
  else :
    delo_e1j1=round(K11*(0-p12))
    delo_e1j2=round(K12*(0-p12))
    delo_e2j1=round(K21*(1-p21))
    delo_e2j2=round(K22*(1-p21))
    # Incrementation des victoires
    ELO[e2j1][1][d]+=pond2
    ELO[e2j2][1][d]+=pond2
    # Incrementation du nb de but quand on perd
    ELO[e1j1][2][d]+=score1*pond1
    ELO[e1j2][2][d]+=score1*pond1
  # ELO
  ELO[e1j1][-1][d]+=delo_e1j1*pond1
  ELO[e1j2][-1][d]+=delo_e1j2*pond1
  ELO[e2j1][-1][d]+=delo_e2j1*pond2
  ELO[e2j2][-1][d]+=delo_e2j2*pond2
  # dELO
  ELO[e1j1][6].append(delo_e1j1)
  ELO[e2j1][6].append(delo_e2j1)
  if e1j2!=e1j1:
    ELO[e1j2][6].append(delo_e1j2)
  if e2j2!=e2j1:
    ELO[e2j2][6].append(delo_e2j2)
  # Victoire ou defaite
  #F_VD(ELO,e1j1,e1j2,score1,e2j1,e2j2,score2)
  # Incrementation des participations
  ELO[e1j1][0][d]+=pond1
  ELO[e1j2][0][d]+=pond1
  ELO[e2j1][0][d]+=pond2
  ELO[e2j2][0][d]+=pond2
  # On retourne le plus petit ELO calcule
  return min(ELO[e1j1][-1][d],ELO[e1j2][-1][d],\
             ELO[e2j1][-1][d],ELO[e2j2][-1][d])



