#!/Produits/publics/x86_64.Linux.RH5/python/2.6.5/bin/python
# /Produits/publics/x86_64.Linux.RH6/python/3.4.1/bin/python
#
# 29/09/17 :
#   Le resultat des matchs avec des nouveaux joueurs n'impacte pas le ELO des
#   anciens joueurs.
# 02/10/17 :
#   Ajout des joueurs de la semaine
# 23/10/17 :
#   Correction des joueurs de la semaine pour que ca corresponde vraiment
#   a une semaine.
# 07/10/17 :
#   Le ELO des equipes tient compte du nb de match min de chaque joueur de
#   l'equipe pour etre pris en compte.
#   Ajout du graphe sur les scores de la semaine.
# 16/01/18 :
#   Calcul des moyennes sur les x=6*30 derniers matchs et affichage de deux
#   graphes (un depuis le debut et un sur les x derniers matchs).
# 09/03/18 :
#   K moyenne dans l'equipe pour appliquer les points.
# 14/12/18 :
#   Correction pour prendre en compte pile au nombre de match joue le changement
#   du coefficient K.
# 15/02/18 :
#   Ajout du "rival" et deplacement des statistiques "coequipiers", "betes
#   noires" sur 6 mois au lieu de depuis le debut.
#
#   A FAIRE !!!!
#
import math
import csv
import sys
import datetime
import pdb # pdb.set_trace()

# DEBUT des fonctions =========================================================

# ---------------
# Fonction printf
# ---------------

def printf(format, *args):
  sys.stdout.write(format % args)

# ----------------
# Fonction sprintf
# ----------------

def sprintf(format, *args):
  return format % args

# Fonction K (facteur dependant du nb de partie jouee et du classement)
# K=40 pour les 30 premiers matchs
# K=20 pour des elo inferieurs a 2400
# K=10 pour les autres

def F_K(nb,elo) :
  if nb < 30 :
    K=40
  elif elo < 2400 :
    K=20
  else :
    K=10
  return K

# -----------------------------------------------
# Fonction pour ajouter un joueur (ou une equipe)
# -----------------------------------------------
# ELO     : dictionnaire contenant tous les joueurs
# nom     : joueur a ajouter
# elo_deb : ELO de depart
# nb      : nombre d'element

def F_ajoutJoueur(ELO,nom,elo_deb,nb) :
  # Besoin de l'ajouter ?
  if not nom in ELO.keys() :
    # Definition des noms des joueurs, du nombre de matchs joue (0), du
    # nb de victoires (1), du nb de buts marques lors des defaites (2), des
    # records (3 - status, perso min, perso max), du numero des matchs joues
    # (4), victoire ou defaite (5), l'evolution du ELO (6) et du elo (dernier
    # tuple obligatoirement)
    ELO[nom]=([0],[0],[0],[0,elo_deb,elo_deb],[0],[str()],[0],[0])
    # Pour les premiers match, je mets "elo_deb", sinon 0 (pour
    # l'affichage, pour ne pas avoir plusieurs lignes qui coupent
    # tout le graphe)
    if nb<10 : ELO[nom][-1][-1]=elo_deb
    for d in range(nb-1) :
      ELO[nom][-1].append(ELO[nom][-1][-1])
      ELO[nom][ 0].append(ELO[nom][ 0][-1])
      ELO[nom][ 1].append(ELO[nom][ 1][-1])
      ELO[nom][ 2].append(ELO[nom][ 2][-1])
    # Initialisation des deux derniers elo a "elo_deb"
    ELO[nom][-1][-1]=elo_deb
    ELO[nom][-1][-2]=elo_deb

#
# ----------------------------------------------------------------
# Fonction pour stocker les victoires ou defaites de chaque joueur
# ----------------------------------------------------------------
# ELO     : dictionnaire contenant tous les joueurs
# e1j1    : equipe 1 joueur 1
# e1j2    : equipe 1 joueur 2
# score1  : score de l'equipe 1
# e2j1    : equipe 2 joueur 1
# e2j2    : equipe 2 joueur 2
# score2  : score de l'equipe 2

def F_VD(ELO,e1j1,e1j2,score1,e2j1,e2j2,score2) :

  if score2>score1 :
    F_VD(ELO,e2j1,e2j2,score2,e1j1,e1j2,score1) 
  else:
    if (e1j1!=e1j2)&(e2j1!=e2j2):
      ELO[e1j1][5][0]+="V";
      ELO[e1j1][5].append(\
        sprintf("[[image:icon:accept||title=\"%s - %02d/%02d - %s %s (%+02d)\"]]",\
        e1j2,score1,score2,e2j1,e2j2,ELO[e1j1][6][-1]));
      ELO[e1j2][5][0]+="V";
      ELO[e1j2][5].append(\
        sprintf("[[image:icon:accept||title=\"%s - %02d/%02d - %s %s (%+02d)\"]]",\
        e1j1,score1,score2,e2j1,e2j2,ELO[e1j2][6][-1]));
      ELO[e2j1][5][0]+="D";
      ELO[e2j1][5].append(\
        sprintf("[[image:icon:cancel||title=\"%s - %02d/%02d - %s %s (%+02d)\"]]",\
        e2j2,score2,score1,e1j1,e1j2,ELO[e2j1][6][-1]));
      ELO[e2j2][5][0]+="D";
      ELO[e2j2][5].append(\
        sprintf("[[image:icon:cancel||title=\"%s - %02d/%02d - %s %s (%+02d)\"]]",\
        e2j1,score2,score1,e1j1,e1j2,ELO[e2j2][6][-1]));
    elif (e1j1!=e1j2)&(e2j1==e2j2):
      ELO[e1j1][5][0]+="V";
      ELO[e1j1][5].append(\
        sprintf("[[image:icon:accept||title=\"%s - %02d/%02d - %s (%+02d)\"]]",\
        e1j2,score1,score2,e2j1,ELO[e1j1][6][-1]));
      ELO[e1j2][5][0]+="V";
      ELO[e1j2][5].append(\
        sprintf("[[image:icon:accept||title=\"%s - %02d/%02d - %s (%+02d)\"]]",\
        e1j1,score1,score2,e2j1,ELO[e1j2][6][-1]));
      ELO[e2j1][5][0]+="D";
      ELO[e2j1][5].append(\
        sprintf("[[image:icon:cancel||title=\"%02d/%02d - %s %s (%+02d)\"]]",\
        score2,score1,e1j1,e1j2,ELO[e2j1][6][-1]));
    elif (e1j1==e1j2)&(e2j1!=e2j2):
      ELO[e1j1][5][0]+="V";
      ELO[e1j1][5].append(\
        sprintf("[[image:icon:accept||title=\"%02d/%02d - %s %s (%+02d)\"]]",\
        score1,score2,e2j1,e2j2,ELO[e1j1][6][-1]));
      ELO[e2j1][5][0]+="D";
      ELO[e2j1][5].append(\
        sprintf("[[image:icon:cancel||title=\"%s - %02d/%02d - %s (%+02d)\"]]",\
        e2j2,score2,score1,e1j1,ELO[e2j1][6][-1]));
      ELO[e2j2][5][0]+="D";
      ELO[e2j2][5].append(\
        sprintf("[[image:icon:cancel||title=\"%s - %02d/%02d - %s (%+02d)\"]]",\
        e2j1,score2,score1,e1j1,ELO[e2j2][6][-1]));
    else:
      ELO[e1j1][5][0]+="V";
      ELO[e1j1][5].append(\
        sprintf("[[image:icon:accept||title=\"%02d/%02d - %s (%+02d)\"]]",\
        score1,score2,e2j1,ELO[e1j1][6][-1]));
      ELO[e2j1][5][0]+="D";
      ELO[e2j1][5].append(\
        sprintf("[[image:icon:cancel||title=\"%02d/%02d - %s (%+02d)\"]]",\
        score2,score1,e1j1,ELO[e2j1][6][-1]));
  # Fin de F_VD

# -------------------------
# Fonction de calcul du ELO
# -------------------------
# ELO    : dictionnaire contenant tous les joueurs
# e1j1   : equipe 1 - joueur 1
# e1j2   : equipe 1 - joueur 2
# score1 : score de l'equipe 1
# e2j1   : equipe 2 - joueur 1
# e2j2   : equipe 2 - joueur 2
# score2 : score de l'equipe 2
# d      : date a calculer
# dnb    : date indiquant le nombre de matchs pour definir l'affectation ou
#          non du score.
# Cette fonction retourne le plus petit des ELO calcule

def F_ELO(ELO,e1j1,e1j2,score1,e2j1,e2j2,score2,d,dnb) :
  # facteur lie a la difference de score
  # fac=abs(score1-score2)/5
  # Correction si les deux joueurs d'une meme equipe sont identiques
  if e1j1 == e1j2 :
    pond1=0.5
  else :
    pond1=1.0
  if e2j1 == e2j2 :
    pond2=0.5
  else :
    pond2=1.0
  # Calcul du elo moyen
  elo1=0.5*(ELO[e1j1][-1][d-1]+ELO[e1j2][-1][d-1])
  elo2=0.5*(ELO[e2j1][-1][d-1]+ELO[e2j2][-1][d-1])
  # Nouveau elo
  D12 =elo1-elo2
  D21 =elo2-elo1
  p12 =1/(1+math.pow(10,-D12/400))
  p21 =1/(1+math.pow(10,-D21/400))
  # K ?
  K11=F_K(ELO[e1j1][0][dnb],ELO[e1j1][-1][d-1])
  K12=F_K(ELO[e1j2][0][dnb],ELO[e1j2][-1][d-1])
  K21=F_K(ELO[e2j1][0][dnb],ELO[e2j1][-1][d-1])
  K22=F_K(ELO[e2j2][0][dnb],ELO[e2j2][-1][d-1])
  # On ne comptabilise les scores que si tous les joueurs ont un coefficient
  # K inferieur a 40, sinon, on ne comptabilise que pour les joueurs ayant
  # un coeff de 40
  if (K11==40)|(K12==40)|(K21==40)|(K22==40) :
    if K11!=40 : K11=0
    if K12!=40 : K12=0
    if K21!=40 : K21=0
    if K22!=40 : K22=0
  else :
    K11=K12=(K11+K12)/2
    K21=K22=(K21+K22)/2
  # Victoire ou defaite : correction des scores
  if score1 > score2 :
    delo_e1j1=round(K11*(1-p12))
    delo_e1j2=round(K12*(1-p12))
    delo_e2j1=round(K21*(0-p21))
    delo_e2j2=round(K22*(0-p21))
    # Incrementation des victoires
    ELO[e1j1][1][d]+=pond1
    ELO[e1j2][1][d]+=pond1
    # Incrementation du nb de but quand on perd
    ELO[e2j1][2][d]+=score2*pond2
    ELO[e2j2][2][d]+=score2*pond2
  else :
    delo_e1j1=round(K11*(0-p12))
    delo_e1j2=round(K12*(0-p12))
    delo_e2j1=round(K21*(1-p21))
    delo_e2j2=round(K22*(1-p21))
    # Incrementation des victoires
    ELO[e2j1][1][d]+=pond2
    ELO[e2j2][1][d]+=pond2
    # Incrementation du nb de but quand on perd
    ELO[e1j1][2][d]+=score1*pond1
    ELO[e1j2][2][d]+=score1*pond1
  # ELO
  ELO[e1j1][-1][d]+=delo_e1j1*pond1
  ELO[e1j2][-1][d]+=delo_e1j2*pond1
  ELO[e2j1][-1][d]+=delo_e2j1*pond2
  ELO[e2j2][-1][d]+=delo_e2j2*pond2
  # dELO
  ELO[e1j1][6].append(delo_e1j1)
  ELO[e2j1][6].append(delo_e2j1)
  if e1j2!=e1j1:
    ELO[e1j2][6].append(delo_e1j2)
  if e2j2!=e2j1:
    ELO[e2j2][6].append(delo_e2j2)
  # Victoire ou defaite
  F_VD(ELO,e1j1,e1j2,score1,e2j1,e2j2,score2)
  # Incrementation des participations
  ELO[e1j1][0][d]+=pond1
  ELO[e1j2][0][d]+=pond1
  ELO[e2j1][0][d]+=pond2
  ELO[e2j2][0][d]+=pond2
  # On retourne le plus petit ELO calcule
  return min(ELO[e1j1][-1][d],ELO[e1j2][-1][d],\
             ELO[e2j1][-1][d],ELO[e2j2][-1][d])

# ------------------------------
# Fonction de calcul des records
# ------------------------------
# rec    : tableau des records min et max
# ELO    : dictionnaire contenant tous les joueurs
# d      : date a calculer
#
def F_records(rec,ELO,d) :
  # Premiere boucle pour trouver les nouveaux records min et max
  for joueur in ELO.keys() :
    if(ELO[joueur][-1][d]>0) :
      # Global
      if(ELO[joueur][-1][d]<rec[0]) :
        rec[0]=ELO[joueur][-1][d]
      if(ELO[joueur][-1][d]>rec[1]) :
        rec[1]=ELO[joueur][-1][d]
      # Perso
      if(ELO[joueur][-1][d]<ELO[joueur][3][1]) :
        ELO[joueur][3][1]=ELO[joueur][-1][d]
      if(ELO[joueur][-1][d]>ELO[joueur][3][2]) :
        ELO[joueur][3][2]=ELO[joueur][-1][d]
  # Seconde boucle pour statuer sur l'evolution des joueurs
  for joueur in ELO.keys() :
    if(((ELO[joueur][-1][d]==rec[0])&(ELO[joueur][-1][d-1]!=rec[0]))|\
       ((ELO[joueur][-1][d]==rec[1])&(ELO[joueur][-1][d-1]!=rec[1]))) :
      ELO[joueur][3][0]=3
    elif((ELO[joueur][-1][d  ]==ELO[joueur][3][1])&\
         (ELO[joueur][-1][d-1]!=ELO[joueur][3][1])) :
      ELO[joueur][3][0]=1
    elif((ELO[joueur][-1][d  ]==ELO[joueur][3][2])&\
         (ELO[joueur][-1][d-1]!=ELO[joueur][3][2])) :
      ELO[joueur][3][0]=2
    else :
      ELO[joueur][3][0]=0

# -----------------------------------
# Fonction de calcul des betes noires
# -----------------------------------
# BN     : dictionnaire contenant les betes noires
# e1j1   : equipe 1 - joueur 1
# e1j2   : equipe 1 - joueur 2
# score1 : score de l'equipe 1
# e2j1   : equipe 2 - joueur 1
# e2j2   : equipe 2 - joueur 2
# score2 : score de l'equipe 2
#

def F_BN(BN,e1j1,e1j2,score1,e2j1,e2j2,score2) :
  # Correction si les deux joueurs d'une meme equipe sont identiques
  if e1j1 == e1j2 :
    pond1=0.5
  else :
    pond1=1.0
  if e2j1 == e2j2 :
    pond2=0.5
  else :
    pond2=1.0
  # 
  if score1 < score2 : s= 1
  else               : s=-1
  #
  for j1 in [e1j1,e1j2] :
    for j2 in [e2j1,e2j2] :
      # Defaite
      nom=j1+" "+j2
      if not nom in BN.keys() :
        BN[nom]=0
      BN[nom]+=s*pond1
      # Victoire
      nom=j2+" "+j1
      if not nom in BN.keys() :
        BN[nom]=0
      BN[nom]-=s*pond2

# -------------------------------------------------
# Fonction d'affichage des matchs a une date donnee
# -------------------------------------------------
# CSV     : dictionnaire contenant tous les matchs
# date    : date a afficher

def F_match(CSV,date) :
  # Recherche de la date
  id=CSV['date'].index(date)
  # On compte le nb de date
  nd=CSV['date'].count(date)
  for i in range(id,id+nd):
    printf(" %d",CSV['match'][i])
    printf(" %-20s",CSV['e1j1'][i]+' '+CSV['e1j2'][i])
    if CSV['score1'][i]>CSV['score2'][i]: printf("*")
    printf(" %2d",CSV['score1'][i])
    printf(" %-20s",CSV['e2j1'][i]+' '+CSV['e2j2'][i])
    if CSV['score1'][i]<CSV['score2'][i]: printf("*")
    printf(" %2d",CSV['score2'][i])
    printf(" %s",CSV['comm'][i])
    printf("\n")

# ----------------------------------
# Fonction d'affichage du classement
# ----------------------------------
# titre     : chaine de caractere du titre du tableau
# ELO       : dictionnaire contenant les scores
# d         : date
# nb        : nb de jours joues sur lesquels on calcule les statistiques
# graphique : 0 = pas de graphique
#             1 = graphique
# drap      : 0 = pas de footnote
#             1 = footnote
# records   : 0 = pas d'affichage des records
#             1 = affichage des records avec la legende
#             2 = affichage des records seuls

def F_classement(titre,ELO,d,nb,graphique,drap,records) :
  # A partir du jour courant, je recherche les nb matchs precedents
  if d<0 :
    d=len(Date)+d
  if (nb<=0)|(nb>d) :
    nb=d
  #
  # Recuperation des derniers scores
  scores_anc=[]
  scores_val=[]
  scores_nom=[]
  scores_nbM=[]
  scores_nbV=[]
  scores_but=[]
  scorei_nbM=[]
  scorei_nbV=[]
  scorei_but=[]
  scores_rec=[]
  scores_vic=[]
  scores_img=[]
  for nom in ELO.keys() :
    scores_nbM.append(ELO[nom][0][d])
    scores_nbV.append(ELO[nom][1][d])
    scores_but.append(ELO[nom][2][d])
    scorei_nbM.append(ELO[nom][0][d]-ELO[nom][0][d-nb])
    scorei_nbV.append(ELO[nom][1][d]-ELO[nom][1][d-nb])
    scorei_but.append(ELO[nom][2][d]-ELO[nom][2][d-nb])
    scores_rec.append(ELO[nom][3][0])
    scores_anc.append(ELO[nom][-1][d-1])
    scores_val.append(ELO[nom][-1][d])
    scores_vic.append(ELO[nom][5][0][-5:])
    scores_img.append("")
    for i in ELO[nom][5][-5:]:
      scores_img[-1]+=i
    scores_nom.append(nom)

  # On trie les derniers scores
  s=sorted(range(len(scores_val)), key=lambda k: scores_val[k], reverse=True)

  # Affichage des scores
  printf("%-20s   %10s  %5s\n","--------------------","----------","-----")
  printf("%-20s : %10s  %5s (nb parties/victoires/buts)\n",titre,Date[-1],"Prog.")
  printf("%-20s   %10s  %5s\n","--------------------","----------","-----")
  for i in s :
    if scores_nbM[i] > 0 :
      prog=scores_val[i]-scores_anc[i]
      printf("%-20s : %10d",scores_nom[i],scores_val[i])
      printf(" %+5d",prog)
      printf(" (%2d",scores_nbM[i])
      if nb<d : printf(" %2d",scorei_nbM[i])
      printf(" /%2d%%",100*scores_nbV[i]/scores_nbM[i])
      if (nb<d)&(scorei_nbM[i]>0) :
        printf(" %2d%%",100*scorei_nbV[i]/scorei_nbM[i])
      if scores_nbM[i]-scores_nbV[i] == 0 :
        printf(" /%.0f",10);
      else :
        printf(" /%.1f",1.*scores_but[i]/(scores_nbM[i]-scores_nbV[i]))
      if nb<d :
        if scorei_nbM[i]-scorei_nbV[i] == 0 :
          printf(" %.0f",10);
        else :
          printf(" %.1f",1.*scorei_but[i]/(scorei_nbM[i]-scorei_nbV[i]))
      printf(")")
      if  (scores_rec[i]==3) : printf("(*)")
      elif(scores_rec[i]==2) : printf("(on)")
      elif(scores_rec[i]==1) : printf("(off)")
      printf(" %5s",scores_vic[i])
      printf("\n")

  if drap == 1 :
    printf("(Nb buts = nombre de buts moyen comptabilise que sur les defaites)\n")
    if nb<d :
     printf("(Les nombres entre parentheses sont calcules a partir du %s)\n",Date[d-nb])

  if records == 1 :
    printf("(*)(nouveau record absolu, en haut ou en bas du classement ;))\n")
    printf("(on)(nouveau record max personnel)\n")
    printf("(off)(nouveau record min personnel)\n")

    #pdb.set_trace()

  # Affichage du graphique
  if graphique == 1 :
    printf("\n")
    printf("{{chart type=\"xy_line_and_shape\"")
    printf(" params=\"range:B2-..")
    printf(";dataset:timetable_xy;domain_axis_type:date")
    printf(";domain_axis_date_format:dd MMM yyyy;date_format:yyyy-MM-dd")
    printf(";colors:ff5555,5555ff,55ff55,c58c48,ff55ff,55ffff")
    printf(",ffafaf,808080,c00000")
    printf(";time_period:day;range_axis_lower:1700;range_axis_upper:2200\"")
    printf(" width=\"600\"}}\n")
    printf("|=Date")
    for j in s :
      printf("|=%s",scores_nom[j])
    printf("\n")
    for date in Date :
      printf("|%s",date.replace("/","-")) 
      for j in s :
        printf("|%d",ELO[scores_nom[j]][-1][Date.index(date)])
      printf("\n")
    printf("{{/chart}}\n")
    if(nb<d) :
      printf("{{chart type=\"xy_line_and_shape\"")
      printf(" params=\"range:B2-..")
      printf(";dataset:timetable_xy;domain_axis_type:date")
      printf(";domain_axis_date_format:dd MMM yyyy;date_format:yyyy-MM-dd")
      printf(";colors:ff5555,5555ff,55ff55,c58c48,ff55ff,55ffff,ffafaf,808080,c00000")
      printf(";time_period:day;range_axis_lower:1550;range_axis_upper:2400\"")
      printf(" width=\"600\"}}\n")
      printf("|=Date")
      for j in s :
        printf("|=%s",scores_nom[j])
      printf("\n")
      for date in Date[(d-nb):d+1] :
        printf("|%s",date.replace("/","-"))
        for j in s :
          printf("|%d",ELO[scores_nom[j]][-1][Date.index(date)])
        printf("\n")
      printf("{{/chart}}\n")

# --------------------------------------------
# Fonction d'affichage du joueur de la semaine
# --------------------------------------------
# ELO       : dictionnaire contenant les scores
# Date      : dictionnaire contenant les dates
# dc        : date courante
# nb        : nb de semaines a afficher
# graphique : 0 = pas de graphique
#             1 = graphique

def F_jSemaine(ELO,Date,dc,nb,graphique) :
  # A partir du jour courant, je recherche les n precedentes semaines
  if dc<0 :
    i=len(Date)+dc
  else :
    i=dc
  ns         =0
  periode    =[0]*(nb+1)
  semaine    =[0]*(nb+1)
  jour       =[0]*(nb+1)
  periode[ns]=i
  semaine[ns]=datetime.datetime.strptime(Date[i],"%Y/%m/%d").isocalendar()[1]
  jour[ns]   =datetime.datetime.strptime(Date[i],"%Y/%m/%d").weekday()
  i         -=1
  ns        +=1
  while (i>0)&(ns<nb+1) :
    periode[ns]=i
    semaine[ns]=datetime.datetime.strptime(Date[i],"%Y/%m/%d").isocalendar()[1]
    jour[ns]   =datetime.datetime.strptime(Date[i],"%Y/%m/%d").weekday()
    if semaine[ns]!=semaine[ns-1] :
      ns+=1
    i-=1
  # 
  printf("%-21s","---------------------")
  printf(" %10s %7s","----------","-------")
  printf(" %10s %7s\n","----------","-------")
  for isem in range(0,ns-1,1) :
    printf("%-21s",sprintf("%s (%s)",Date[periode[isem]],jours[jour[isem]]))
    # Recherche du meilleur et du pire joueurs pour cette date
    d1    =periode[isem]
    d2    =periode[isem+1]
    joueur=[]
    jscore=[]
    imax  =0
    for nom in ELO.keys() :
      try :
        if((ELO[nom][-1][d2]>0)&(ELO[nom][0][d2]>=30)) :
          imax+=1
          joueur.append(nom)
          jscore.append(ELO[nom][-1][d1]-ELO[nom][-1][d2])
      except :
        pass
    # Si aucun joueur, pas de classement a faire
    if(len(jscore)==0) :
      printf(" %10s (%5d)"," -",0)
      printf(" %10s (%5d)\n"," -",0)
      if isem==0 : graphique=0
    else :
      # On trie les derniers scores
      s=sorted(range(len(jscore)), key=lambda k: jscore[k], reverse=True)
      # Meilleurs scores
      nomA=joueur[s[0]]
      i=1
      while (jscore[s[i]]==jscore[s[0]]) :
        nomA=sprintf("%s/%s",nomA,joueur[s[i]])
        i+=1
        if(i==imax) : break
      # Pire scores
      nomB=joueur[s[-1]]
      i=-2;
      while (jscore[s[i]]==jscore[s[-1]]) :
        nomB=sprintf("%s/%s",nomB,joueur[s[i]])
        if(i==-imax) : break
        i-=1
      printf(" %10s (%5d)",nomA,jscore[s[0]])
      printf(" %10s (%5d)\n",nomB,jscore[s[-1]])
      # Affichage du graphique - recuperation des donnees
      if (isem==0)&(graphique==1) :
        data_nom=[]
        data_val=[]
        data_day=[]
        data_n  =d1-d2+1
        data_j  =0
        data_day.append(6)
        for d in range(d2+1,d1+1,1) :
          data_day.append(\
           datetime.datetime.strptime(Date[d],"%Y/%m/%d").weekday())
        for j in s :
          data_j+=1
          data_nom.append(joueur[j])
          data_val.append([0])
          for d in range(d2+1,d1+1,1) :
            data_val[-1].append(ELO[data_nom[-1]][-1][d]-\
                                ELO[data_nom[-1]][-1][d2])
  if (graphique==1)&(ns>1) :
    printf("\n")
    printf("(%% style=\"text-align:center\" %%)\n")
    printf("__Semaine en cours__\n")
    printf("{{chart type=\"line\"")
    printf(" params=\"range:B2-..")
    printf(";series:columns")
    printf(";colors:ff5555,5555ff,55ff55,c58c48,ff55ff,55ffff")
    printf(",ffafaf,808080,c00000")
    printf("\"")
    printf(" width=\"400\"}}\n")
    printf("|=Date")
    for nom in data_nom :
      printf("|=%s",nom)
    printf("\n")
    for n in range(0,data_n,1) :
      printf("|%s",jours[data_day[n]])
      for j in range(0,data_j,1) :
        printf("|%d",data_val[j][n])
      printf("\n")
    printf("{{/chart}}\n")
    #pdb.set_trace()

# -----------------------------------------------------------------------------
# Fonction d'affichage des coequipiers, du defit, de la bete noires et du rival
# -----------------------------------------------------------------------------
# ELOj      : dictionnaire contenant les scores par joueur
# ELOe      : dictionnaire contenant les scores par equipe
# BN        : dictionnaire des betes noires
# d         : date
# nb        : nb de jours joues sur lesquels on calcule les statistiques
# Date      : tableau contenant toutes les dates

def F_cdbr(ELOj,ELOe,BN,d,nb,Date):
  # A partir du jour courant, je recherche les nb dates precedentes
  if d<0 :
    d=len(Date)+d
  if (nb<=0)|(nb>d) :
    nb=d

  npremiers=2
  nbBetes  =3
  nbRivaux =2

  # -----------------
  # Calcul des rivaux
  # -----------------
  RV={}
  # Initialisation
  for j1 in ELOj.keys():
    for j2 in ELOj.keys():
      if j1!=j2:
        j1j2=j1+" "+j2
        RV[j1j2]=[0,0]
        if ELO[j1][-1][d-nb]>ELO[j2][-1][d-nb]:
          RV[j1j2][0]=1
        elif ELO[j1][-1][d-nb]==ELO[j2][-1][d-nb]:
          RV[j1j2][0]=0
        else:
          RV[j1j2][0]=-1
  # Boucle sur toutes les dates et on verifie s'il y a croisement
  for i in range(d-nb+1,d+1,1):
    for j1 in ELOj.keys():
      for j2 in ELOj.keys():
        if j1!=j2:
          j1j2=j1+" "+j2
          if (ELO[j1][-1][i]>ELO[j2][-1][i])and(RV[j1j2][0]!=1):
            RV[j1j2][0] = 1
            RV[j1j2][1]+= 1
          elif (ELO[j1][-1][i]==ELO[j2][-1][i])and(RV[j1j2][0]!=0):
            RV[j1j2][0] = 0
            RV[j1j2][1]+= 1
          elif (ELO[j1][-1][i]<ELO[j2][-1][i])and(RV[j1j2][0]!=-1):
            RV[j1j2][0] =-1
            RV[j1j2][1]+= 1

  printf("%-20s   %10s\n","--------------------","-----------")
  printf("%-20s : %10s\n","Joueurs","Coequipiers (occurences)")
  printf("%-20s   %10s\n","--------------------","-----------")
  for joueur in ELOj.keys() :
    occurences=[]
    coequipier=[]
    equipesElo=[]
    for equipe in ELOe.keys() :
      # On cherche si le joueur appartient a l'equipe
      i=equipe.find(joueur)
      if i != -1 :
        # On cherche le nom du coequipier
        if i == 0 :
          co=equipe[(equipe.find(" "))+1:]
        else :
          co=equipe[0:(i-1)]
        # Si ce n'est pas lui-meme on stocke les occurences
        if joueur != co :
          occurences.append(ELOe[equipe][0][d]-ELOe[equipe][0][d-nb])
          equipesElo.append(ELOe[equipe][-1][d])
          coequipier.append(co)
    # On trie de la plus petite vers la plus grande
    s=sorted(range(len(occurences)), key=lambda k: occurences[k], reverse=False)
    # On trie du plus petit nb de victoire au plus grand
    v=sorted(range(len(equipesElo)), key=lambda k: equipesElo[k], reverse=False)
    # On cherche les betes noires du joueur
    nbDefaite=[]
    beteNoire=[]
    for i in range(nbBetes) :
      nbDefaite.append(-100)
      beteNoire.append("-")
    for jb in ELOj.keys() :
      nom=joueur+" "+jb
      if nom in BN.keys() :
        for j in range(nbBetes-1,-1,-1) :
          if nbDefaite[j] < BN[nom] :
            for i in range(j) :
              nbDefaite[i]=nbDefaite[i+1]
              beteNoire[i]=beteNoire[i+1]
            nbDefaite[j]=BN[nom]
            beteNoire[j]=jb
            break
          elif nbDefaite[j] == BN[nom] :
            beteNoire[j]=sprintf("%s/%s",beteNoire[j],jb)
            break
    # On cherche les rivaux du joueur
    nbCoupe=[]
    rivaux =[]
    for i in range(nbRivaux) :
      nbCoupe.append(-1)
      rivaux.append("-")
    for jb in ELOj.keys() :
      nom=joueur+" "+jb
      if nom in RV.keys() :
        for j in range(nbRivaux-1,-1,-1) :
          if nbCoupe[j] < RV[nom][1] :
            for i in range(j) :
              nbCoupe[i]=nbCoupe[i+1]
              rivaux[i] =rivaux[i+1]
            nbCoupe[j]=RV[nom][1]
            rivaux[j] =jb
            break
          elif nbCoupe[j] == RV[nom][1] :
            rivaux[j]=sprintf("%s/%s",rivaux[j],jb)
            break
    # On affiche
    printf("%-20s :",joueur)
    for i in s[0:npremiers] :
      printf(" %10s (%2d)",coequipier[i],occurences[i])
    printf(" (%10s - %d)",coequipier[v[0]],equipesElo[v[0]])
    printf(" <")
    for i in range(nbBetes-1,-1,-1) :
      if nbDefaite[i] > 0 :
        printf(" %s (%d)",beteNoire[i],nbDefaite[i])
      else :
        printf(" %s (%d)",beteNoire[i],nbDefaite[i])
        break
    printf(" >")
    printf(" <")
    for i in range(nbRivaux-1,-1,-1) :
      if nbCoupe[i] > 0 :
        printf(" %s (%d)",rivaux[i],nbCoupe[i])
      else :
        break
    printf(" >")
    printf("\n")


# FIN des fonctions ===========================================================

# Initialisation

jours      =["Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi","Dimanche"]
Date       =['2015/11/16']
nb_date    =1
elo_deb    =2000
ELO        ={}
ELO_equipes={}
BN         ={}
CSV        ={'match':[],'date':[],'e1j1':[],'e1j2':[],'e2j1':[],'e2j2':[],
             'score1':[],'score2':[],'comm':[]}
rec_joueur =[elo_deb,elo_deb]
rec_equipe =[elo_deb,elo_deb]
nabsent    =50
nJoursJoues=3*30

# Lecture du fichier CSV contenant l'evolution des scores et determination
# du ELO (K=40 et la regle des 400 points d'ecart pour la non prise en compte
# des resultats)

# Ouverture du fichier

fic_csv='./bf_scores.csv'
fid    =open(fic_csv,"r")

# On saute la premiere ligne

fid.next()
#next(fid)

# Lecture et calculs

# Creation du lecteur CSV
matchs=csv.reader(fid)

# Boucle sur tous les matchs
nm=0
for jeu in matchs :
  date  =jeu[0]
  e1j1  =jeu[1]
  e1j2  =jeu[2]
  e2j1  =jeu[3]
  e2j2  =jeu[4]
  score1=int(jeu[5])
  score2=int(jeu[6])
  if len(jeu)>7:
    comm=jeu[7]
  else:
    comm=''
  # On stocke tous les matchs lu
  nm=nm+1;
  CSV['match' ].append(nm)
  CSV['date'  ].append(date)
  CSV['e1j1'  ].append(e1j1)
  CSV['e1j2'  ].append(e1j2)
  CSV['e2j1'  ].append(e2j1)
  CSV['e2j2'  ].append(e2j2)
  CSV['score1'].append(score1)
  CSV['score2'].append(score2)
  CSV['comm'  ].append(comm)
  # On identifie la date
  d=-1
  # Nouvelle date ?
  if not date in Date :
    # On calcule les records du jour passe
    F_records(rec_joueur,ELO,d)
    F_records(rec_equipe,ELO_equipes,d)
    # On rajoute une colonne (en recopiant la derniere valeur)
    for nom in ELO.keys() :
      ELO[nom][-1].append(ELO[nom][-1][-1])
      ELO[nom][ 0].append(ELO[nom][ 0][-1])
      ELO[nom][ 1].append(ELO[nom][ 1][-1])
      ELO[nom][ 2].append(ELO[nom][ 2][-1])
    for nom in ELO_equipes.keys() :
      ELO_equipes[nom][-1].append(ELO_equipes[nom][-1][-1])
      ELO_equipes[nom][ 0].append(ELO_equipes[nom][ 0][-1])
      ELO_equipes[nom][ 1].append(ELO_equipes[nom][ 1][-1])
      ELO_equipes[nom][ 2].append(ELO_equipes[nom][ 2][-1])
    # On insere la date
    Date.append(date)
    # On incremente le compteur
    nb_date+=1
  # Nouveau nom ?
  F_ajoutJoueur(ELO,e1j1,elo_deb,nb_date)
  F_ajoutJoueur(ELO,e1j2,elo_deb,nb_date)
  F_ajoutJoueur(ELO,e2j1,elo_deb,nb_date)
  F_ajoutJoueur(ELO,e2j2,elo_deb,nb_date)
  if (not e1j1+" "+e1j2 in ELO_equipes.keys())&\
     (not e1j2+" "+e1j1 in ELO_equipes.keys()) :
    F_ajoutJoueur(ELO_equipes,e1j1+" "+e1j2,elo_deb,nb_date)
  if (not e2j1+" "+e2j2 in ELO_equipes.keys())&\
     (not e2j2+" "+e2j1 in ELO_equipes.keys()) :
    F_ajoutJoueur(ELO_equipes,e2j1+" "+e2j2,elo_deb,nb_date)
  # Determination du nouveau ELO
  elo_min=F_ELO(ELO,e1j1,e1j2,score1,e2j1,e2j2,score2,d,d)
  # On corrige le elo de debut pour les eventuels nouveaux joueurs
  # if nb_date > 5 :
  #   elo_deb=min(elo_deb,elo_min)
  # Pour les equipes, on cherche la bonne combinaison des noms
  if ELO_equipes.has_key(e1j1+" "+e1j2) :
    e1=e1j1+" "+e1j2
  else :
    e1=e1j2+" "+e1j1
  if ELO_equipes.has_key(e2j1+" "+e2j2) :
    e2=e2j1+" "+e2j2
  else :
    e2=e2j2+" "+e2j1
  # La prise en compte du score depend du nb de matchs de chaque
  # des joueurs et pas de celui de l'equipe
  ELO_equipes[e1][0][0]=min([ELO[e1j1][0][d-1],ELO[e1j2][0][d-1]])
  ELO_equipes[e2][0][0]=min([ELO[e2j1][0][d-1],ELO[e2j2][0][d-1]])
  F_ELO(ELO_equipes,e1,e1,score1,e2,e2,score2,d,0)
  # Determination des "betes noires" pour chaque joueur
  F_BN(BN,e1j1,e1j2,score1,e2j1,e2j2,score2)
  # On stocke le numero du dernier match joue
  ELO[e1j1][4].append(nm)
  ELO[e2j1][4].append(nm)
  if e1j2!=e1j1:
    ELO[e1j2][4].append(nm)
  if e2j2!=e2j1:
    ELO[e2j2][4].append(nm)
  ELO_equipes[e1][4].append(nm)
  ELO_equipes[e2][4].append(nm)

# Fermeture du fichier lu

fid.close()

# Recalcule de la bete noire sur un nb limite de match
if(nJoursJoues>0):
  BN={}
  for im in range(CSV["date"].index(Date[d-nJoursJoues]),nm,1):
    F_BN(BN,CSV["e1j1"][im],CSV["e1j2"][im],CSV["score1"][im],\
            CSV["e2j1"][im],CSV["e2j2"][im],CSV["score2"][im])

# On calcule les records du jour courant
F_records(rec_joueur,ELO,d)
F_records(rec_equipe,ELO_equipes,d)

# Creation des equipes qui n'ont jamais ete formee

liste_joueurs=[]
for nom in ELO.keys() :
  liste_joueurs.append(nom)
nb_joueurs=len(liste_joueurs)

for i in range(0,nb_joueurs) :
  for j in range(i,nb_joueurs) :
    if (not liste_joueurs[i]+" "+liste_joueurs[j] in ELO_equipes.keys())&\
       (not liste_joueurs[j]+" "+liste_joueurs[i] in ELO_equipes.keys()) :
      F_ajoutJoueur(ELO_equipes,liste_joueurs[i]+" "+liste_joueurs[j],\
                    elo_deb,nb_date)

# Suppression des joueurs absents de plus de nabsent matchs

for nom in ELO.keys() :
  if ELO[nom][4][-1]<nm-nabsent :
    # On supprime le joueur ainsi que toutes les equipes auquel il
    del ELO[nom] 
    # Ainsi que toutes les equipes auquel il a participe
    for equipe in ELO_equipes.keys() :
      # On cherche si le joueur appartient a l'equipe
      i=equipe.find(nom)
      if i != -1 :
        del ELO_equipes[equipe]

# Affichage des derniers matchs joues

printf(u"Derniers matchs en cours au %s :\n",date)

F_match(CSV,date)

# Affichage du classement pour les joueurs

printf(u"Classement :\n")

F_classement("Joueurs",ELO,d,nJoursJoues,0,1,1)

# Affichage du joueur de la semaine

printf(u"Joueur de la semaine :\n")

F_jSemaine(ELO,Date,d,5,0)

# Affichage du classement pour les equipes

#F_classement("Equipes",ELO_equipes,d,0,0)

# Recherche des equipes a former

F_cdbr(ELO,ELO_equipes,BN,d,nJoursJoues,Date)
