from setuptools import setup

setup(
    name='baby',
    packages=['baby'],
    include_package_data=True,
    install_requires=[
        'flask','matplotlib','pandas'
    ],
)
